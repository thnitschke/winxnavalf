﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Media;

namespace WinxNavalFuncional
{
    public partial class TelaInicial : Form
    {
        public TelaInicial()
        {
            InitializeComponent();
            ContinueButton.Enabled = File.Exists("./savedgame.txt");

            /*SoundPlayer player = new SoundPlayer
            {
                SoundLocation = @"audio\winxfunk.wav"
            };
            player.PlayLooping(); */
        }

        public Ajuda Ajuda1
        {
            get => default(Ajuda);
            set
            {
            }
        }

        public Posicionar Posicionar
        {
            get => default(Posicionar);
            set
            {
            }
        }

        public TeladeJogo TeladeJogo
        {
            get => default(TeladeJogo);
            set
            {
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {   //Ajuda
            Form telaAjuda = new Ajuda();
            telaAjuda.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {   //Sair
            this.Dispose();
        }

        private void NewGamebutton_Click(object sender, EventArgs e)
        {
            Posicionar posicionar1 = new Posicionar();
            DialogResult res = posicionar1.ShowDialog();
            this.Hide();
            while (res == DialogResult.Cancel && Posicionar.reposicionar)
            {
                posicionar1 = new Posicionar();
                res = posicionar1.ShowDialog();
            }
            if(res == DialogResult.Cancel && Posicionar.newgame)
            {
                TeladeJogo jogo = new TeladeJogo(Posicionar.listaNavios);
                jogo.ShowDialog();

                fimjogo();
            }
            this.Show();
        }

        private void Continue_Click(object sender, EventArgs e)
        {
            TeladeJogo jogo = new TeladeJogo("savedgame");
            jogo.ShowDialog();
            fimjogo();
            this.Show();
        }

        private void fimjogo()
        {
            
            if (Partida.finalizada == 1)
            {
                EndGame win = new EndGame(true);
                win.ShowDialog();
            }
            else if(Partida.finalizada == 2)
            {
                EndGame lose = new EndGame(false);
                lose.ShowDialog();
            }
        }
    }
}
