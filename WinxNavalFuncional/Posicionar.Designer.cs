﻿namespace WinxNavalFuncional
{
    partial class Posicionar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem("Porta-Aviões");
            System.Windows.Forms.ListViewItem listViewItem2 = new System.Windows.Forms.ListViewItem("Destroyer");
            System.Windows.Forms.ListViewItem listViewItem3 = new System.Windows.Forms.ListViewItem("Destroyer");
            System.Windows.Forms.ListViewItem listViewItem4 = new System.Windows.Forms.ListViewItem("Submarino");
            System.Windows.Forms.ListViewItem listViewItem5 = new System.Windows.Forms.ListViewItem("Submarino");
            System.Windows.Forms.ListViewItem listViewItem6 = new System.Windows.Forms.ListViewItem("Submarino");
            this.tabuleiroPanel = new System.Windows.Forms.TableLayoutPanel();
            this.NaviosLista = new System.Windows.Forms.ListView();
            this.JogarButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.Rotacionar = new System.Windows.Forms.Button();
            this.buttonVoltar = new System.Windows.Forms.Button();
            this.buttonReposicionar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tabuleiroPanel
            // 
            this.tabuleiroPanel.ColumnCount = 10;
            this.tabuleiroPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tabuleiroPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tabuleiroPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tabuleiroPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tabuleiroPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tabuleiroPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tabuleiroPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tabuleiroPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tabuleiroPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tabuleiroPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tabuleiroPanel.Cursor = System.Windows.Forms.Cursors.Cross;
            this.tabuleiroPanel.Location = new System.Drawing.Point(12, 12);
            this.tabuleiroPanel.Name = "tabuleiroPanel";
            this.tabuleiroPanel.RowCount = 10;
            this.tabuleiroPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tabuleiroPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tabuleiroPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tabuleiroPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tabuleiroPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tabuleiroPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tabuleiroPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tabuleiroPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tabuleiroPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tabuleiroPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tabuleiroPanel.Size = new System.Drawing.Size(300, 300);
            this.tabuleiroPanel.TabIndex = 0;
            this.tabuleiroPanel.CellPaint += new System.Windows.Forms.TableLayoutCellPaintEventHandler(this.tabuleiroPanel_CellPaint);
            this.tabuleiroPanel.MouseEnter += new System.EventHandler(this.tabuleiroPanel_MouseEnter);
            this.tabuleiroPanel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.tabuleiroPanel_MouseMove);
            this.tabuleiroPanel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.tabuleiroPanel_MouseUp);
            // 
            // NaviosLista
            // 
            this.NaviosLista.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem1,
            listViewItem2,
            listViewItem3,
            listViewItem4,
            listViewItem5,
            listViewItem6});
            this.NaviosLista.Location = new System.Drawing.Point(330, 54);
            this.NaviosLista.MultiSelect = false;
            this.NaviosLista.Name = "NaviosLista";
            this.NaviosLista.Size = new System.Drawing.Size(89, 110);
            this.NaviosLista.TabIndex = 1;
            this.NaviosLista.UseCompatibleStateImageBehavior = false;
            this.NaviosLista.View = System.Windows.Forms.View.List;
            this.NaviosLista.MouseUp += new System.Windows.Forms.MouseEventHandler(this.NaviosLista_MouseUp);
            // 
            // JogarButton
            // 
            this.JogarButton.Enabled = false;
            this.JogarButton.Location = new System.Drawing.Point(330, 226);
            this.JogarButton.Name = "JogarButton";
            this.JogarButton.Size = new System.Drawing.Size(92, 23);
            this.JogarButton.TabIndex = 2;
            this.JogarButton.Text = "Jogar!";
            this.JogarButton.UseVisualStyleBackColor = true;
            this.JogarButton.Click += new System.EventHandler(this.JogarButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(327, 12);
            this.label1.MaximumSize = new System.Drawing.Size(100, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 26);
            this.label1.TabIndex = 3;
            this.label1.Text = "Posicione as Winx no tabuleiro.";
            // 
            // Rotacionar
            // 
            this.Rotacionar.Location = new System.Drawing.Point(330, 170);
            this.Rotacionar.Name = "Rotacionar";
            this.Rotacionar.Size = new System.Drawing.Size(89, 23);
            this.Rotacionar.TabIndex = 4;
            this.Rotacionar.Text = "Rotacionar";
            this.Rotacionar.UseVisualStyleBackColor = true;
            this.Rotacionar.Click += new System.EventHandler(this.Rotacionar_Click);
            // 
            // buttonVoltar
            // 
            this.buttonVoltar.Location = new System.Drawing.Point(330, 255);
            this.buttonVoltar.Name = "buttonVoltar";
            this.buttonVoltar.Size = new System.Drawing.Size(92, 23);
            this.buttonVoltar.TabIndex = 5;
            this.buttonVoltar.Text = "Voltar";
            this.buttonVoltar.UseVisualStyleBackColor = true;
            this.buttonVoltar.Click += new System.EventHandler(this.buttonVoltar_Click);
            // 
            // buttonReposicionar
            // 
            this.buttonReposicionar.Location = new System.Drawing.Point(330, 284);
            this.buttonReposicionar.Name = "buttonReposicionar";
            this.buttonReposicionar.Size = new System.Drawing.Size(92, 23);
            this.buttonReposicionar.TabIndex = 6;
            this.buttonReposicionar.Text = "Reposicionar";
            this.buttonReposicionar.UseVisualStyleBackColor = true;
            this.buttonReposicionar.Click += new System.EventHandler(this.buttonReposicionar_Click);
            // 
            // Posicionar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(445, 325);
            this.Controls.Add(this.buttonReposicionar);
            this.Controls.Add(this.buttonVoltar);
            this.Controls.Add(this.Rotacionar);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.JogarButton);
            this.Controls.Add(this.NaviosLista);
            this.Controls.Add(this.tabuleiroPanel);
            this.Name = "Posicionar";
            this.Text = "Posicionar";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tabuleiroPanel;
        private System.Windows.Forms.ListView NaviosLista;
        private System.Windows.Forms.Button JogarButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Rotacionar;
        private System.Windows.Forms.Button buttonVoltar;
        private System.Windows.Forms.Button buttonReposicionar;
    }
}