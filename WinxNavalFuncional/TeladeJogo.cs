﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinxNavalFuncional
{
    public partial class TeladeJogo : Form
    {
        Partida partida;
        TableLayoutPanelCellPosition posAtual;
        private bool voltar;
        private Color[,] AdversarioGridbgColors;

        private Color[,] meuGridbgColors;

        public TeladeJogo(String filename)
        {
            InitializeComponent();
            partida = new Partida(filename);

            AdversarioGridbgColors = new Color[10, 10];
            meuGridbgColors = new Color[10, 10];
            voltar = false;
            pintaGradeAdversario();
            pintaMinhaGrade();
            /*System.Diagnostics.Debug.WriteLine("Minha grade");
            partida.minhaGrade.print();
            System.Diagnostics.Debug.WriteLine("Adversario");
            partida.gradeAdversarioREAL.print();*/
            atualizaLabels();
            
        }

        internal TeladeJogo(List<Navio> meusBarcos)
        {
            InitializeComponent();
            partida = new Partida(10, 0, 0, 6, 6, meusBarcos);
            
            AdversarioGridbgColors = new Color[10, 10];
            meuGridbgColors = new Color[10, 10];
            pintaGradeAdversario();
            pintaMinhaGrade();
            voltar = false;
            /*System.Diagnostics.Debug.WriteLine("Minha grade");
            partida.minhaGrade.print();
            System.Diagnostics.Debug.WriteLine("Adversario");
            partida.gradeAdversarioREAL.print();*/
            atualizaLabels();
        }

        private void atualizaLabels()
        {
            labelRodada.Text = partida.contadorRodada.ToString();
            labelMeusAcertos.Text = partida.contadorAcertos.ToString();
            labelBarcosAdvRest.Text = partida.contadorBarcosAdversarios.ToString();
            labelBarcosMeusRest.Text = partida.contadorBarcosMeus.ToString();
        }

        private void resetGrids()
        {
            for(int i=0; i < tabuleiroAdversarioPanel.ColumnCount; i++)
            {
                for(int j=0; j<tabuleiroAdversarioPanel.RowCount; j++)
                {
                    meuGridbgColors[i, j] = Color.Blue;
                    AdversarioGridbgColors[i, j] = Color.Blue;
                }
            }
        }

        internal static List<Navio> listaNavios { get; set; }
        public static bool Win { get; private set; }

        private void tabuleiroAdversarioPanel_MouseMove(object sender, MouseEventArgs e)
        {
            pintaCelulasAdversario();
        }

        private void tabuleiroAdversarioPanel_MouseEnter(object sender, EventArgs e)
        {
            pintaCelulasAdversario();
        }

        private void pintaGradeAdversario()
        {
            for (int i = 0; i < partida.gradeAdversario.tabuleiro.GetLength(0); i++)
            {
                for (int j = 0; j < partida.gradeAdversario.tabuleiro.GetLength(1); j++)
                {

                    if (partida.gradeAdversario.tabuleiro[i, j].isDead)
                        AdversarioGridbgColors[i, j] = Color.Brown;
                    else if (partida.gradeAdversario.tabuleiro[i, j].isBurning)
                        AdversarioGridbgColors[i, j] = Color.Red;
                    else if (partida.gradeAdversario.tabuleiro[i, j].Discovered)
                        AdversarioGridbgColors[i, j] = Color.DarkBlue;
                    else
                        AdversarioGridbgColors[i, j] = Color.Blue;
                }
            }
        }

        private void pintaCelulasAdversario()
        {
            if (posAtual != GetCellPosition(tabuleiroAdversarioPanel))
            {
                posAtual = GetCellPosition(tabuleiroAdversarioPanel);
                pintaGradeAdversario();
                AdversarioGridbgColors[posAtual.Column, posAtual.Row] = Color.Purple;
                tabuleiroAdversarioPanel.Refresh();
                
            }
        }

        private void pintaMinhaGrade()
        {
            for (int i = 0; i < partida.minhaGrade.tabuleiro.GetLength(0); i++)
            {
                for (int j = 0; j < partida.minhaGrade.tabuleiro.GetLength(1); j++)
                {

                    if (partida.minhaGrade.tabuleiro[i, j].isDead)
                        meuGridbgColors[i, j] = Color.Brown;
                    else if (partida.minhaGrade.tabuleiro[i, j].isBurning)
                        meuGridbgColors[i, j] = Color.Red;
                    else if (partida.minhaGrade.tabuleiro[i, j].temNavio)
                        meuGridbgColors[i, j] = Color.LimeGreen;
                    else if (partida.minhaGrade.tabuleiro[i, j].Discovered)
                        meuGridbgColors[i, j] = Color.DarkBlue;
                    else
                        meuGridbgColors[i, j] = Color.Blue;
                }
            }
            meuTabuleiroPanel.Refresh();
        }

        //The method to get the position of the cell under the mouse.
        private TableLayoutPanelCellPosition GetCellPosition(TableLayoutPanel panel)
        {
            //mouse position
            Point p = panel.PointToClient(Control.MousePosition);
            //Cell position
            TableLayoutPanelCellPosition pos = new TableLayoutPanelCellPosition(0, 0);
            //Panel size.
            Size size = panel.Size;
            //average cell size.
            SizeF cellAutoSize = new SizeF(size.Width / panel.ColumnCount, size.Height / panel.RowCount);

            //Get the cell row.
            //y coordinate
            float y = 0;
            for (int i = 0; i < panel.RowCount; i++)
            {
                //Calculate the summary of the row heights.
                SizeType type = panel.RowStyles[i].SizeType;
                float height = panel.RowStyles[i].Height;
                switch (type)
                {
                    case SizeType.Absolute:
                        y += height;
                        break;
                    case SizeType.Percent:
                        y += height / 100 * size.Height;
                        break;
                    case SizeType.AutoSize:
                        y += cellAutoSize.Height;
                        break;
                }
                //Check the mouse position to decide if the cell is in current row.
                if ((int)y > p.Y)
                {
                    pos.Row = i;
                    break;
                }
            }

            //Get the cell column.
            //x coordinate
            float x = 0;
            for (int i = 0; i < panel.ColumnCount; i++)
            {
                //Calculate the summary of the row widths.
                SizeType type = panel.ColumnStyles[i].SizeType;
                float width = panel.ColumnStyles[i].Width;
                switch (type)
                {
                    case SizeType.Absolute:
                        x += width;
                        break;
                    case SizeType.Percent:
                        x += width / 100 * size.Width;
                        break;
                    case SizeType.AutoSize:
                        x += cellAutoSize.Width;
                        break;
                }
                //Check the mouse position to decide if the cell is in current column.
                if ((int)x > p.X)
                {
                    pos.Column = i;
                    break;
                }
            }

            //return the mouse position.
            return pos;
        }

        private void TeladeJogo_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(!voltar)
                Application.Exit();
        }

        private void buttonVoltar_Click(object sender, EventArgs e)
        {
            voltar = true;
            this.Dispose();
        }

        private void meuTabuleiroPanel_CellPaint(object sender, TableLayoutCellPaintEventArgs e)
        {
            using (var b = new SolidBrush(meuGridbgColors[e.Column, e.Row]))
            {
                e.Graphics.FillRectangle(b, e.CellBounds);
            }
        }

        private void tabuleiroAdversarioPanel_CellPaint(object sender, TableLayoutCellPaintEventArgs e)
        {
            using (var b = new SolidBrush(AdversarioGridbgColors[e.Column, e.Row]))
            {
                e.Graphics.FillRectangle(b, e.CellBounds);
            }
        }

        private void buttonContinuarDepois_Click(object sender, EventArgs e)
        {
            partida.save("savedgame");
            this.Dispose();
        }

        private void tabuleiroAdversarioPanel_MouseUp(object sender, MouseEventArgs e)
        {
            partida.Turno(new Coordinate(posAtual.Column, posAtual.Row));
            pintaCelulasAdversario();
            pintaMinhaGrade();
            atualizaLabels();
            if(Partida.finalizada != 0)
            {
                Win = Partida.finalizada == 1;
                voltar = true;
                this.Dispose();
            }
        }

        internal Navio Navio
        {
            get => default(Navio);
            set
            {
            }
        }

        public EndGame EndGame
        {
            get => default(EndGame);
            set
            {
            }
        }

        internal Partida Partida
        {
            get => default(Partida);
            set
            {
            }
        }
    }
}
