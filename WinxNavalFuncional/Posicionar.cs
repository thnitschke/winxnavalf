﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinxNavalFuncional
{
    public partial class Posicionar : Form
    {
        public static bool reposicionar { get; private set; }
        public static bool newgame { get; private set; }

        private bool atualPosicionavel;
        private Tabuleiro grade;
        internal static List<Navio> listaNavios;
        Navio barcoAtual;
        TableLayoutPanelCellPosition posAtual;
        Color[,] GridbgColors;

        internal List<Navio> ListaNavios { get => listaNavios; set => listaNavios = value; }
        

        public Posicionar()
        {
            InitializeComponent();
            reposicionar = false;
            GridbgColors = new Color[10,10];
            grade = new Tabuleiro(10);
            atualPosicionavel = false;
            ListaNavios = new List<Navio>();
            NaviosLista.Items[0].Selected = true;
            NaviosLista.AccessibilityObject.ToString(); //workaround to get selecteditems properties refreshed

            barcoAtual = NaviofromText(NaviosLista.SelectedItems[0].Text);
            resetGrid();

            newgame = false;
            reposicionar = false;
        }

        private void resetGrid()
        {
            for (int i = 0; i < tabuleiroPanel.ColumnCount; i++)
            {
                for (int j = 0; j < tabuleiroPanel.RowCount; j++)
                {
                    GridbgColors[i, j] = Color.Blue;
                }
            }
            tabuleiroPanel.Refresh();
        }

        private void tabuleiroPanel_MouseMove(object sender, MouseEventArgs e)
        {
            pintaCelulas();
        }

        private void tabuleiroPanel_MouseEnter(object sender, EventArgs e)
        {
            pintaCelulas();
        }

        private void pintaCelulas()
        {
            if (posAtual != GetCellPosition(tabuleiroPanel))
            {
                pintaGrade();
                if (barcoAtual != null)
                {
                    posAtual = GetCellPosition(tabuleiroPanel);

                    if (barcoAtual.possivelPosicionar(posAtual, grade))
                    {   //se for possível, pinta todas as células alcançáveis pelo navio de verde
                        atualPosicionavel = true;
                        for (int i = 0; i < barcoAtual.length; i++)
                        {
                            if (barcoAtual.orientation) //deitado
                            {
                                GridbgColors[posAtual.Column + i, posAtual.Row] = Color.LimeGreen;
                            }
                            else        //de pé
                            {
                                GridbgColors[posAtual.Column, posAtual.Row + i] = Color.LimeGreen;
                            }
                        }
                    }
                    else
                    {   //senão pinta as celulas de vermelho
                        atualPosicionavel = false;
                        for (int i = 0; i < barcoAtual.length; i++)
                        {
                            if (barcoAtual.orientation) //deitado
                            {
                                if (posAtual.Column + i < 10)
                                    GridbgColors[posAtual.Column + i, posAtual.Row] = Color.Red;
                            }
                            else        //de pé
                            {
                                if (posAtual.Row + i < 10)
                                    GridbgColors[posAtual.Column, posAtual.Row + i] = Color.Red;
                            }
                        }
                    }
                    tabuleiroPanel.Refresh();
                }
            }
        }

        private void pintaGrade()
        {
            for (int i =0; i< grade.tabuleiro.GetLength(0); i++)
            {
                for(int j = 0; j < grade.tabuleiro.GetLength(1); j++)
                {

                    if (grade.tabuleiro[i, j].temNavio)
                        GridbgColors[i,j] = Color.Green;
                    else
                        GridbgColors[i, j] = Color.Blue;
                }
            }
        }

        //The method to get the position of the cell under the mouse.
        private TableLayoutPanelCellPosition GetCellPosition(TableLayoutPanel panel)
        {
            //mouse position
            Point p = panel.PointToClient(Control.MousePosition);
            //Cell position
            TableLayoutPanelCellPosition pos = new TableLayoutPanelCellPosition(0, 0);
            //Panel size.
            Size size = panel.Size;
            //average cell size.
            SizeF cellAutoSize = new SizeF(size.Width / panel.ColumnCount, size.Height / panel.RowCount);

            //Get the cell row.
            //y coordinate
            float y = 0;
            for (int i = 0; i < panel.RowCount; i++)
            {
                //Calculate the summary of the row heights.
                SizeType type = panel.RowStyles[i].SizeType;
                float height = panel.RowStyles[i].Height;
                switch (type)
                {
                    case SizeType.Absolute:
                        y += height;
                        break;
                    case SizeType.Percent:
                        y += height / 100 * size.Height;
                        break;
                    case SizeType.AutoSize:
                        y += cellAutoSize.Height;
                        break;
                }
                //Check the mouse position to decide if the cell is in current row.
                if ((int)y > p.Y)
                {
                    pos.Row = i;
                    break;
                }
            }

            //Get the cell column.
            //x coordinate
            float x = 0;
            for (int i = 0; i < panel.ColumnCount; i++)
            {
                //Calculate the summary of the row widths.
                SizeType type = panel.ColumnStyles[i].SizeType;
                float width = panel.ColumnStyles[i].Width;
                switch (type)
                {
                    case SizeType.Absolute:
                        x += width;
                        break;
                    case SizeType.Percent:
                        x += width / 100 * size.Width;
                        break;
                    case SizeType.AutoSize:
                        x += cellAutoSize.Width;
                        break;
                }
                //Check the mouse position to decide if the cell is in current column.
                if ((int)x > p.X)
                {
                    pos.Column = i;
                    break;
                }
            }

            //return the mouse position.
            return pos;
        }

        private Navio NaviofromText(String text)
        {
            switch (text)
            {
                case "Porta-Aviões":
                    return new PortaAviao();
                case "Submarino":
                    return new Submarino();
                case "Destroyer":
                    return new Destroyer();
                default:
                    return null;
            }
        }

        private void tabuleiroPanel_MouseUp(object sender, MouseEventArgs e)
        {
            if (atualPosicionavel)
            {
                for (int i = 0; i < barcoAtual.length; i++)
                {
                    if (barcoAtual.orientation) //deitado
                    {
                        Celula celulaAtual = grade.tabuleiro[posAtual.Column + i, posAtual.Row];
                        grade.tabuleiro[posAtual.Column + i, posAtual.Row] = new Celula(true, celulaAtual.isDead, celulaAtual.isBurning, celulaAtual.Discovered, celulaAtual.barco);
                        GridbgColors[posAtual.Column + i, posAtual.Row] = Color.Green;
                    }
                    else        //de pé
                    {
                        Celula celulaAtual = grade.tabuleiro[posAtual.Column, posAtual.Row + i];
                        grade.tabuleiro[posAtual.Column, posAtual.Row + i] = new Celula(true, celulaAtual.isDead, celulaAtual.isBurning, celulaAtual.Discovered, celulaAtual.barco);
                        GridbgColors[posAtual.Column, posAtual.Row + i] = Color.Green;
                    }
                }
                barcoAtual.setPosition(posAtual.Column, posAtual.Row);
                System.Diagnostics.Debug.WriteLine (barcoAtual.pos.x);
                listaNavios.Add(barcoAtual);
                NaviosLista.Items.Remove(NaviosLista.SelectedItems[0]);
                if (NaviosLista.Items.Count > 0)
                {
                    NaviosLista.Items[0].Selected = true;
                    barcoAtual = NaviofromText(NaviosLista.SelectedItems[0].Text);
                }
                else
                {
                    barcoAtual = null;
                    Rotacionar.Enabled = false;
                    JogarButton.Enabled = true;
                    tabuleiroPanel.Enabled = false;
                    NaviosLista.Enabled = false;
                } 
                
            }
        }

        private void tabuleiroPanel_CellPaint(object sender, TableLayoutCellPaintEventArgs e)
        {
            using (var b = new SolidBrush(GridbgColors[e.Column, e.Row]))
            {
                e.Graphics.FillRectangle(b, e.CellBounds);
            }
        }

        private void Rotacionar_Click(object sender, EventArgs e) => barcoAtual.orientation = !barcoAtual.orientation;

        private void NaviosLista_MouseUp(object sender, MouseEventArgs e)
        {
            if (NaviosLista.FocusedItem != null)
            {
                if (NaviosLista.SelectedItems.Count == 0) {
                    NaviosLista.FocusedItem.Selected = true;
                    
                }
            }
            barcoAtual = NaviofromText(NaviosLista.SelectedItems[0].Text);
        }

        private void buttonVoltar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void buttonReposicionar_Click(object sender, EventArgs e)
        {
            reposicionar = true;
            this.Dispose();
        }

        private void JogarButton_Click(object sender, EventArgs e)
        {
            newgame = true;
            TeladeJogo.listaNavios = listaNavios;
            this.Dispose();
        }

        internal Navio Navio
        {
            get => default(Navio);
            set
            {
            }
        }

        internal Tabuleiro Tabuleiro
        {
            get => default(Tabuleiro);
            set
            {
            }
        }
    }
}
