﻿namespace WinxNavalFuncional
{
    partial class TeladeJogo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.meuTabuleiroPanel = new System.Windows.Forms.TableLayoutPanel();
            this.tabuleiroAdversarioPanel = new System.Windows.Forms.TableLayoutPanel();
            this.labelMeutab = new System.Windows.Forms.Label();
            this.labelTabAdv = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.labelMeusAcertos = new System.Windows.Forms.Label();
            this.labelBarcosAdvRest = new System.Windows.Forms.Label();
            this.labelBarcosMeusRest = new System.Windows.Forms.Label();
            this.labelRodada = new System.Windows.Forms.Label();
            this.buttonContinuarDepois = new System.Windows.Forms.Button();
            this.buttonVoltar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // meuTabuleiroPanel
            // 
            this.meuTabuleiroPanel.ColumnCount = 10;
            this.meuTabuleiroPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.meuTabuleiroPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.meuTabuleiroPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.meuTabuleiroPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.meuTabuleiroPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.meuTabuleiroPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.meuTabuleiroPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.meuTabuleiroPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.meuTabuleiroPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.meuTabuleiroPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 57F));
            this.meuTabuleiroPanel.Cursor = System.Windows.Forms.Cursors.Cross;
            this.meuTabuleiroPanel.Location = new System.Drawing.Point(26, 30);
            this.meuTabuleiroPanel.Name = "meuTabuleiroPanel";
            this.meuTabuleiroPanel.RowCount = 10;
            this.meuTabuleiroPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.meuTabuleiroPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.meuTabuleiroPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.meuTabuleiroPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.meuTabuleiroPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.meuTabuleiroPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.meuTabuleiroPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.meuTabuleiroPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.meuTabuleiroPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.meuTabuleiroPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.meuTabuleiroPanel.Size = new System.Drawing.Size(300, 300);
            this.meuTabuleiroPanel.TabIndex = 1;
            this.meuTabuleiroPanel.CellPaint += new System.Windows.Forms.TableLayoutCellPaintEventHandler(this.meuTabuleiroPanel_CellPaint);
            // 
            // tabuleiroAdversarioPanel
            // 
            this.tabuleiroAdversarioPanel.ColumnCount = 10;
            this.tabuleiroAdversarioPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tabuleiroAdversarioPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tabuleiroAdversarioPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tabuleiroAdversarioPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tabuleiroAdversarioPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tabuleiroAdversarioPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tabuleiroAdversarioPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tabuleiroAdversarioPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tabuleiroAdversarioPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tabuleiroAdversarioPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 57F));
            this.tabuleiroAdversarioPanel.Cursor = System.Windows.Forms.Cursors.Cross;
            this.tabuleiroAdversarioPanel.Location = new System.Drawing.Point(340, 30);
            this.tabuleiroAdversarioPanel.Name = "tabuleiroAdversarioPanel";
            this.tabuleiroAdversarioPanel.RowCount = 10;
            this.tabuleiroAdversarioPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tabuleiroAdversarioPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tabuleiroAdversarioPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tabuleiroAdversarioPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tabuleiroAdversarioPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tabuleiroAdversarioPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tabuleiroAdversarioPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tabuleiroAdversarioPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tabuleiroAdversarioPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tabuleiroAdversarioPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tabuleiroAdversarioPanel.Size = new System.Drawing.Size(300, 300);
            this.tabuleiroAdversarioPanel.TabIndex = 2;
            this.tabuleiroAdversarioPanel.CellPaint += new System.Windows.Forms.TableLayoutCellPaintEventHandler(this.tabuleiroAdversarioPanel_CellPaint);
            this.tabuleiroAdversarioPanel.MouseEnter += new System.EventHandler(this.tabuleiroAdversarioPanel_MouseEnter);
            this.tabuleiroAdversarioPanel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.tabuleiroAdversarioPanel_MouseMove);
            this.tabuleiroAdversarioPanel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.tabuleiroAdversarioPanel_MouseUp);
            // 
            // labelMeutab
            // 
            this.labelMeutab.AutoSize = true;
            this.labelMeutab.Location = new System.Drawing.Point(136, 14);
            this.labelMeutab.Name = "labelMeutab";
            this.labelMeutab.Size = new System.Drawing.Size(64, 13);
            this.labelMeutab.TabIndex = 3;
            this.labelMeutab.Text = "Meu Campo";
            // 
            // labelTabAdv
            // 
            this.labelTabAdv.AutoSize = true;
            this.labelTabAdv.Location = new System.Drawing.Point(451, 14);
            this.labelTabAdv.Name = "labelTabAdv";
            this.labelTabAdv.Size = new System.Drawing.Size(93, 13);
            this.labelTabAdv.TabIndex = 4;
            this.labelTabAdv.Text = "Campo Adversário";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(660, 91);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(75, 13);
            this.label40.TabIndex = 5;
            this.label40.Text = "Meus Acertos:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(660, 133);
            this.label1.MaximumSize = new System.Drawing.Size(100, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 26);
            this.label1.TabIndex = 6;
            this.label1.Text = "Barcos adversários restantes:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(660, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Rodada:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(660, 181);
            this.label3.MaximumSize = new System.Drawing.Size(100, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 26);
            this.label3.TabIndex = 8;
            this.label3.Text = "Barcos meus restantes:";
            // 
            // labelMeusAcertos
            // 
            this.labelMeusAcertos.AutoSize = true;
            this.labelMeusAcertos.Location = new System.Drawing.Point(703, 108);
            this.labelMeusAcertos.Name = "labelMeusAcertos";
            this.labelMeusAcertos.Size = new System.Drawing.Size(13, 13);
            this.labelMeusAcertos.TabIndex = 9;
            this.labelMeusAcertos.Text = "0";
            // 
            // labelBarcosAdvRest
            // 
            this.labelBarcosAdvRest.AutoSize = true;
            this.labelBarcosAdvRest.Location = new System.Drawing.Point(703, 159);
            this.labelBarcosAdvRest.Name = "labelBarcosAdvRest";
            this.labelBarcosAdvRest.Size = new System.Drawing.Size(13, 13);
            this.labelBarcosAdvRest.TabIndex = 10;
            this.labelBarcosAdvRest.Text = "0";
            // 
            // labelBarcosMeusRest
            // 
            this.labelBarcosMeusRest.AutoSize = true;
            this.labelBarcosMeusRest.Location = new System.Drawing.Point(703, 207);
            this.labelBarcosMeusRest.Name = "labelBarcosMeusRest";
            this.labelBarcosMeusRest.Size = new System.Drawing.Size(13, 13);
            this.labelBarcosMeusRest.TabIndex = 11;
            this.labelBarcosMeusRest.Text = "0";
            // 
            // labelRodada
            // 
            this.labelRodada.AutoSize = true;
            this.labelRodada.Location = new System.Drawing.Point(718, 45);
            this.labelRodada.Name = "labelRodada";
            this.labelRodada.Size = new System.Drawing.Size(13, 13);
            this.labelRodada.TabIndex = 12;
            this.labelRodada.Text = "0";
            // 
            // buttonContinuarDepois
            // 
            this.buttonContinuarDepois.Location = new System.Drawing.Point(673, 248);
            this.buttonContinuarDepois.Name = "buttonContinuarDepois";
            this.buttonContinuarDepois.Size = new System.Drawing.Size(75, 37);
            this.buttonContinuarDepois.TabIndex = 13;
            this.buttonContinuarDepois.Text = "Continuar Depois";
            this.buttonContinuarDepois.UseVisualStyleBackColor = true;
            this.buttonContinuarDepois.Click += new System.EventHandler(this.buttonContinuarDepois_Click);
            // 
            // buttonVoltar
            // 
            this.buttonVoltar.Location = new System.Drawing.Point(673, 291);
            this.buttonVoltar.Name = "buttonVoltar";
            this.buttonVoltar.Size = new System.Drawing.Size(75, 23);
            this.buttonVoltar.TabIndex = 14;
            this.buttonVoltar.Text = "Voltar";
            this.buttonVoltar.UseVisualStyleBackColor = true;
            this.buttonVoltar.Click += new System.EventHandler(this.buttonVoltar_Click);
            // 
            // TeladeJogo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(767, 344);
            this.Controls.Add(this.buttonVoltar);
            this.Controls.Add(this.buttonContinuarDepois);
            this.Controls.Add(this.labelRodada);
            this.Controls.Add(this.labelBarcosMeusRest);
            this.Controls.Add(this.labelBarcosAdvRest);
            this.Controls.Add(this.labelMeusAcertos);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label40);
            this.Controls.Add(this.labelTabAdv);
            this.Controls.Add(this.labelMeutab);
            this.Controls.Add(this.tabuleiroAdversarioPanel);
            this.Controls.Add(this.meuTabuleiroPanel);
            this.Name = "TeladeJogo";
            this.Text = "Winx Naval - Jogo";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.TeladeJogo_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel meuTabuleiroPanel;
        private System.Windows.Forms.TableLayoutPanel tabuleiroAdversarioPanel;
        private System.Windows.Forms.Label labelMeutab;
        private System.Windows.Forms.Label labelTabAdv;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labelMeusAcertos;
        private System.Windows.Forms.Label labelBarcosAdvRest;
        private System.Windows.Forms.Label labelBarcosMeusRest;
        private System.Windows.Forms.Label labelRodada;
        private System.Windows.Forms.Button buttonContinuarDepois;
        private System.Windows.Forms.Button buttonVoltar;
    }
}