﻿namespace WinxNavalFuncional
{
    partial class TelaInicial
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TelaInicial));
            this.NewGamebutton = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.Sair = new System.Windows.Forms.Button();
            this.Ajuda = new System.Windows.Forms.Button();
            this.ContinueButton = new System.Windows.Forms.Button();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // NewGamebutton
            // 
            this.NewGamebutton.Location = new System.Drawing.Point(146, 23);
            this.NewGamebutton.Name = "NewGamebutton";
            this.NewGamebutton.Size = new System.Drawing.Size(83, 46);
            this.NewGamebutton.TabIndex = 0;
            this.NewGamebutton.Text = "New Game";
            this.NewGamebutton.UseVisualStyleBackColor = true;
            this.NewGamebutton.Click += new System.EventHandler(this.NewGamebutton_Click);
            // 
            // button2
            // 
            this.button2.Enabled = false;
            this.button2.Location = new System.Drawing.Point(207, 326);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "Highscore";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // Sair
            // 
            this.Sair.Location = new System.Drawing.Point(297, 326);
            this.Sair.Name = "Sair";
            this.Sair.Size = new System.Drawing.Size(75, 23);
            this.Sair.TabIndex = 3;
            this.Sair.Text = "Sair";
            this.Sair.UseVisualStyleBackColor = true;
            this.Sair.Click += new System.EventHandler(this.button4_Click);
            // 
            // Ajuda
            // 
            this.Ajuda.Location = new System.Drawing.Point(21, 326);
            this.Ajuda.Name = "Ajuda";
            this.Ajuda.Size = new System.Drawing.Size(75, 23);
            this.Ajuda.TabIndex = 4;
            this.Ajuda.Text = "Ajuda";
            this.Ajuda.UseVisualStyleBackColor = true;
            this.Ajuda.Click += new System.EventHandler(this.button5_Click);
            // 
            // ContinueButton
            // 
            this.ContinueButton.Location = new System.Drawing.Point(113, 326);
            this.ContinueButton.Name = "ContinueButton";
            this.ContinueButton.Size = new System.Drawing.Size(75, 23);
            this.ContinueButton.TabIndex = 2;
            this.ContinueButton.Text = "Continue";
            this.ContinueButton.UseVisualStyleBackColor = true;
            this.ContinueButton.Click += new System.EventHandler(this.Continue_Click);
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // Inicial
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(384, 361);
            this.Controls.Add(this.Ajuda);
            this.Controls.Add(this.Sair);
            this.Controls.Add(this.ContinueButton);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.NewGamebutton);
            this.DoubleBuffered = true;
            this.Name = "Inicial";
            this.Text = "Batalha Winx";
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button NewGamebutton;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button Sair;
        private System.Windows.Forms.Button Ajuda;
        private System.Windows.Forms.Button ContinueButton;
        private System.Windows.Forms.ErrorProvider errorProvider1;
    }
}

