﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinxNavalFuncional
{
    abstract class Navio
    {
        public Coordinate pos;
        public bool orientation = true;       //True = deitado / False = de pé
        public int length;  //readonly?

        public void setPosition(int x, int y) => this.pos = new Coordinate(x, y);

        private bool rangeTest(int x, int y, Tabuleiro tabuleiro) {
            if (orientation) {  //deitado
                return (x + length-1 < tabuleiro.range.x) && y < tabuleiro.range.y;
            }
            else               //de pé
            {
                return (y + length-1 < tabuleiro.range.y) && x < tabuleiro.range.x;
            }
        }

        internal bool possivelPosicionar(TableLayoutPanelCellPosition pos, Tabuleiro tabuleiro)
        {
            if(!rangeTest(pos.Column, pos.Row, tabuleiro))
                return false;

            return possivelPosicionar(pos.Column, pos.Row, tabuleiro);
        }

        internal bool possivelPosicionar(int x, int y, Tabuleiro tabuleiro)
        {
            bool test = true;
            int i = 0;

            if (orientation)
            {
                do
                {
                    if (x+i >= 10 || tabuleiro.tabuleiro[x + i, y].temNavio)
                        test = false;
                    i++;
                } while (i < length && test);
            }
            else
            {
                do
                {
                    if (y+i >=10 || tabuleiro.tabuleiro[x, y + i].temNavio)
                        test = false;
                    i++;
                } while (i < length && test);
            }
            return test;
        }

        public void setOrientation(bool neworient) {
            orientation = neworient;
        }

        internal Coordinate[] getCelulas()
        {
            Coordinate[] returnvalue = new Coordinate[length];
            if (orientation == true)
            {
                for (int i = 0; i < length; i++)
                {
                    returnvalue[i] = new Coordinate(pos.x +i, pos.y);
                }
            }
            else
            {
                for (int i = 0; i < length; i++)
                {
                    returnvalue[i] = new Coordinate(pos.x, pos.y + i);
                }
            }
            return returnvalue;
        }

        public override string ToString() => length + "," + pos.x + "," + pos.y + "," + orientation;
    }
}
