﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinxNavalFuncional
{
    class Partida
    {
        public static int finalizada;
        public int tamGrade;
        public int contadorRodada; 
        public int contadorAcertos;
        public int contadorBarcosAdversarios;
        public int contadorBarcosMeus;
        
        public Tabuleiro gradeAdversarioREAL;
        public Tabuleiro gradeAdversario;
        public Tabuleiro minhaGrade;

        List<Navio> BarcosMeus;
        List<Navio> BarcosAdversarios;

        public Partida(int tamG,
                 int cRodada,
                 int cAcertos,
                 int cBarcosAdversarios,
                 int cBarcosMeus,
                 List<Navio> meusBarcos
                 )
        {
            tamGrade = tamG;
            contadorRodada = cRodada;
            contadorAcertos = cAcertos;
            contadorBarcosAdversarios = cBarcosAdversarios;
            contadorBarcosMeus = cBarcosMeus;

            BarcosMeus = meusBarcos;
            BarcosAdversarios = gerarPosicoes();
            geraGrades();
            finalizada = 0;
        }
        
        private void geraGrades(){
                gradeAdversarioREAL = new Tabuleiro(tamGrade, BarcosAdversarios);
                gradeAdversario = new Tabuleiro(tamGrade);
                minhaGrade = new Tabuleiro(tamGrade, BarcosMeus);
        }

        internal static List<Navio> gerarPosicoes()
        {
            List<Navio> returnvalue = new List<Navio>();
            returnvalue.Add(new PortaAviao());
            returnvalue.Add(new Destroyer());
            returnvalue.Add(new Destroyer());
            returnvalue.Add(new Submarino());
            returnvalue.Add(new Submarino());
            returnvalue.Add(new Submarino());

            Random randGen = new Random(System.DateTime.Now.Millisecond);
            Tabuleiro tabuleiro = new Tabuleiro(10);
            int randx, randy;
            foreach (Navio barco in returnvalue)
            {
                do
                {
                    if (randGen.Next(2) == 0)
                        barco.setOrientation(true);
                    else
                        barco.setOrientation(false);

                    randx = randGen.Next(10);
                    randy = randGen.Next(10);

                } while (!barco.possivelPosicionar(randx, randy, tabuleiro));
                barco.setPosition(randx, randy);
                tabuleiro.colocarNavio(barco);
            }

            return returnvalue;
        }

        public void Turno(Coordinate tiroPos)
        {
            System.Diagnostics.Debug.WriteLine("Minha grade");
            minhaGrade.print();
            System.Diagnostics.Debug.WriteLine("Adversario");
            gradeAdversarioREAL.print();
            int tiro = gradeAdversarioREAL.tiro(tiroPos);
            System.Diagnostics.Debug.WriteLine("1 Tiro: "+tiro.ToString());
            if ( tiro != -1) //tiro válido
            {
                gradeAdversario.tabuleiro[tiroPos.x, tiroPos.y] = gradeAdversarioREAL.tabuleiro[tiroPos.x, tiroPos.y];

                if (tiro !=0 ) contadorAcertos++; //se não errou
                if (tiro == 2 && --contadorBarcosAdversarios == 0) //barco afundou
                        finalizada = 1;

                Coordinate tiroRand = minhaGrade.randomTiro();

                tiro = minhaGrade.tiro(tiroRand);
                System.Diagnostics.Debug.WriteLine("2 Tiro: " + tiro.ToString() +"\n");
                if (tiro == 2 && --contadorBarcosMeus == 0)
                        finalizada = 2;
                contadorRodada++;
            }
            
        }

        public Partida(String filename)
        {
            this.fromFile(filename);
        }

        public Partida fromFile(String filename)
        {
            
            FileStream fs = new FileStream(@".\" + filename + ".txt", FileMode.Open, FileAccess.Read);
            using (var streamReader = new StreamReader(fs, Encoding.UTF8))
            {
                string line = streamReader.ReadLine();
                tamGrade = int.Parse(line);
                line = streamReader.ReadLine();
                contadorRodada = int.Parse(line);
                line = streamReader.ReadLine();
                contadorAcertos = int.Parse(line);
                line = streamReader.ReadLine();
                contadorBarcosAdversarios = int.Parse(line);
                line = streamReader.ReadLine();
                contadorBarcosMeus = int.Parse(line);

                line = streamReader.ReadLine();
                BarcosAdversarios = NavioListfromString(line);
                line = streamReader.ReadLine();
                BarcosMeus = NavioListfromString(line);

                geraGrades(); // gera grades com os barcos como se o jogo estivesse no início
                //System.Diagnostics.Debug.WriteLine(minhaGrade.tabuleiro[0, 0].GetBarco().ToString());
                for (int i = 0; i < tamGrade; i++)
                {
                    line = streamReader.ReadLine();
                    for (int j = 0; j < tamGrade; j++)
                    {
                        Navio barco = gradeAdversarioREAL.tabuleiro[j, i].barco;
                        gradeAdversarioREAL.tabuleiro[j, i] = new Celula(line[j], barco);
                    }
                }
                line = streamReader.ReadLine();
                for (int i = 0; i < tamGrade; i++)
                {
                    line = streamReader.ReadLine();
                    for (int j = 0; j < tamGrade; j++)
                    {
                        Navio barco = gradeAdversario.tabuleiro[j, i].barco;
                        gradeAdversario.tabuleiro[j, i] = new Celula(line[j], barco);
                    }
                }
                line = streamReader.ReadLine();
                for (int i = 0; i < tamGrade; i++)
                {
                    line = streamReader.ReadLine();
                    for (int j = 0; j < tamGrade; j++)
                    {
                        Navio barco = minhaGrade.tabuleiro[j, i].barco;
                        minhaGrade.tabuleiro[j, i] = new Celula(line[j], barco);
                    }
                }

                finalizada = 0;
                
            }
            return this;
        }

        private List<Navio> NavioListfromString(string line)
        {
            List<Navio> lista = new List<Navio>();
            foreach (String navioString in line.Split('/'))
            {
                lista.Add(NaviofromString(navioString));
            }
            return lista;
        }

        private Navio NaviofromString(string navioString)
        {
            var atributos = navioString.Split(',');
            switch (int.Parse(atributos[0]))
            {
                default:
                case 1:
                    return new Submarino(int.Parse(atributos[1]), int.Parse(atributos[2]), atributos[3] == "True");
                case 2:
                    return new Destroyer(int.Parse(atributos[1]), int.Parse(atributos[2]), atributos[3] == "True");
                case 4:
                    return new PortaAviao(int.Parse(atributos[1]), int.Parse(atributos[2]), atributos[3] == "True");
            }
        }

        public void save(String filename)
        {
            FileStream fs = new FileStream(@".\"+filename+".txt", FileMode.Create);
            StreamWriter sw = new StreamWriter(fs, Encoding.UTF8);
            sw.WriteLine(tamGrade);
            sw.WriteLine(contadorRodada);
            sw.WriteLine(contadorAcertos);
            sw.WriteLine(contadorBarcosAdversarios);
            sw.WriteLine(contadorBarcosMeus);

            sw.WriteLine(NavioListToString(BarcosAdversarios));
            sw.WriteLine(NavioListToString(BarcosMeus));

            foreach (String line in gradeAdversarioREAL.ToString().Split('\n'))
            {
                sw.WriteLine(line);
            }
            foreach (String line in gradeAdversario.ToString().Split('\n'))
            {
                sw.WriteLine(line);
            }
            foreach (String line in minhaGrade.ToString().Split('\n'))
            {
                sw.WriteLine(line);
            }

            sw.WriteLine();
            sw.Flush();
            sw.Close();
            fs.Close();
        }

        private String NavioListToString(List<Navio> barcos)
        {
            String returnvalue = "";
            for(int i=0; i<barcos.Count; i++)
            {
                returnvalue = returnvalue + barcos[i].ToString();
                if (i < barcos.Count - 1)
                    returnvalue = returnvalue + "/";
            }
            return returnvalue;
        }

        internal Tabuleiro Tabuleiro
        {
            get => default(Tabuleiro);
            set
            {
            }
        }

        internal Coordinate Coordinate
        {
            get => default(Coordinate);
            set
            {
            }
        }
    }
}
