﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinxNavalFuncional
{
    class Destroyer : Navio
    {
        public Destroyer()
        {
            length = 2;
        }

        public Destroyer(int x, int y, bool Orientation)
        {
            length = 2;
            setPosition(x, y);
            orientation = Orientation;
        }
    }
}
