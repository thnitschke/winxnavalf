﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinxNavalFuncional
{
    public partial class EndGame : Form
    {
        public EndGame(bool Win)
        {
            InitializeComponent();
            if (Win)
            {
                Text = "You win!";
                Tag = "You win!";
                //BackgroundImage = new Bitmap(AppDomain.CurrentDomain.BaseDirectory + @"\images\YouWin.png");
            }
            else
            {
                Text = "You lose!";
                Tag = "You lose!";
                //BackgroundImage = new Bitmap(AppDomain.CurrentDomain.BaseDirectory + @"\images\YouLose.png");
            }
        }
    }
}
