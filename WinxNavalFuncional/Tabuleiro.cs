﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinxNavalFuncional
{

    class Tabuleiro
    {
        public Coordinate range;
        public Celula[,] tabuleiro;
        Random randGen;

        public Tabuleiro(int tam)
        {
            range = new Coordinate(tam, tam);
            tabuleiro = new Celula[tam, tam];

            for (int i = 0; i < tam; i++)
            {
                for (int j = 0; j < tam; j++)
                {
                    tabuleiro[i, j] = new Celula();
                }
            }
            randGen = new Random(System.DateTime.Now.Millisecond);
        }

        public Tabuleiro(int tam, List<Navio> Barcos)
        {
            range = new Coordinate(tam, tam);
            tabuleiro = new Celula[tam,tam];

            for (int i = 0; i < tam; i++)
            {
                for (int j = 0; j < tam; j++)
                {
                    tabuleiro[i, j] = new Celula();
                }
            }

            colocarNavios(Barcos);
            randGen = new Random(System.DateTime.Now.Millisecond);
        }

        void setXRange(int x)
        {
            range.x = x;
        }
        void setYRange(int y)
        {
            range.y = y;
        }

        public void print() => System.Diagnostics.Debug.Write(this.ToString());

        public override String ToString()
        {
            String returnvalue = "";

            for (int j = 0; j < range.y; j++)
            {
                for (int i = 0; i < range.x; i++)
                {
                    if (tabuleiro[i, j].Discovered)
                        returnvalue = returnvalue + "X";
                    else if(!tabuleiro[i, j].temNavio)
                        returnvalue = returnvalue + "-";
                    else if (tabuleiro[i, j].isDead)
                        returnvalue = returnvalue + "D";
                    else if(tabuleiro[i, j].isBurning)
                        returnvalue = returnvalue + "F";
                    else
                        returnvalue = returnvalue + "O";
                }
                returnvalue = returnvalue + "\n";
            }
            return returnvalue;
        }

        internal void colocarNavios(List<Navio> barcos)
        {
            foreach(Navio barco in barcos)
            {
                colocarNavio(barco);
            }
        }

        internal void colocarNavio(Navio barco)
        {
            for(int i=0; i<barco.length; i++)
            {
                if (barco.orientation) // deitado
                {
                    if(barco.pos.x+i < range.x)
                    {
                        Celula celulaAtual = tabuleiro[barco.pos.x + i, barco.pos.y];
                        tabuleiro[barco.pos.x + i, barco.pos.y] = new Celula(true, celulaAtual.isDead, celulaAtual.isBurning, celulaAtual.Discovered, barco);
                    }
                }
                else      // de pé
                {
                    if (barco.pos.y + i < range.y)
                    {
                        Celula celulaAtual = tabuleiro[barco.pos.x, barco.pos.y + i];
                        tabuleiro[barco.pos.x, barco.pos.y + i] = new Celula(true, celulaAtual.isDead, celulaAtual.isBurning, celulaAtual.Discovered, barco);
                    }
                }
            }
        }

        internal Coordinate randomTiro()
        {
            Coordinate coord;
            do {
                coord = new Coordinate(randGen.Next()%10, randGen.Next()%10);

            } while (tabuleiro[coord.x, coord.y].isBurning ||
                    tabuleiro[coord.x, coord.y].isDead ||
                    tabuleiro[coord.x, coord.y].Discovered);

            return coord;
        }

        internal int tiro(Coordinate tiro)
        {//returnvalue = -1 tiro inválido
         //              0 se errou
         //              1 se acertou mas não afundou
         //              2 se acertou e afundou

            if (tabuleiro[tiro.x, tiro.y].isBurning || tabuleiro[tiro.x, tiro.y].isDead || tabuleiro[tiro.x, tiro.y].Discovered)
                return -1;

            if (!tabuleiro[tiro.x, tiro.y].temNavio)
            {
                tabuleiro[tiro.x, tiro.y].Discovered = true;
                return 0;
            }

            // se todas as outras celulas pegando fogo, afunda navio
            Celula celulaAtual = tabuleiro[tiro.x, tiro.y];
            tabuleiro[tiro.x, tiro.y] = new Celula(celulaAtual.temNavio, celulaAtual.isDead, true, celulaAtual.Discovered, celulaAtual.barco);

            Coordinate[] celulas = tabuleiro[tiro.x, tiro.y].barco.getCelulas();
            System.Diagnostics.Debug.WriteLine("comprimento" + tabuleiro[tiro.x, tiro.y].barco.length);
            bool morto = true;
            foreach (Coordinate celula in celulas)
                if (!tabuleiro[celula.x, celula.y].isBurning)
                    morto = false;
            if (morto)
            {
                foreach (Coordinate celula in celulas)
                {
                    Celula celulaAgora = tabuleiro[celula.x, celula.y];
                    tabuleiro[celula.x, celula.y] = new Celula(celulaAgora.temNavio, true, celulaAgora.isBurning, celulaAgora.Discovered, celulaAgora.barco);
                }
                return 2;
            }
            else
                return 1;


        }
    }
}
