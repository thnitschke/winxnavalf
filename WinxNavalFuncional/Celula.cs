﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinxNavalFuncional
{
     struct Celula
     {
        public bool temNavio;
        public bool isDead;
        public bool isBurning;
        public Navio barco;

        public bool Discovered { get; internal set; }

        public Navio GetBarco()
        {
            return barco;
        }

        public void SetBarco(Navio value)
        {
            barco = value;
        }

        public Celula(bool temNavio = false, bool isDead = false, bool isBurning = false, bool Discovered = false, Navio barco = null)
        {
            this.temNavio = temNavio;
            this.isDead = isDead;
            this.isBurning = isBurning;
            this.Discovered = Discovered;
            this.barco = barco;
        }

        public Celula(Char c, Navio barco = null)
        {
            switch (c)
            {
                case 'X':
                    this = new Celula(false, false, false, true, barco);
                    break;
                default:
                case '-':
                    this = new Celula(false, false, false, false, barco);
                    break;
                case 'D':
                    this = new Celula(true, true, false, false, barco);
                    break;
                case 'F':
                    this = new Celula(true, false, true, false, barco);
                    break;
                case 'O':
                    this = new Celula(true, false, false, false, barco);
                    break;
            }
        }
    }
}
