﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinxNavalFuncional
{
    class Submarino : Navio
    {
        public Submarino()
        {
            this.length = 1;
        }

        public Submarino(int x, int y, bool Orientation)
        {
            length = 1;
            setPosition(x, y);
            orientation = Orientation;
        }
    }
}
