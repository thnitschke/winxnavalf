﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinxNavalFuncional
{
    class PortaAviao : Navio
    {
        public PortaAviao()
        {
            this.length = 4;
        }

        public PortaAviao(int x, int y, bool Orientation)
        {

            length = 4;
            setPosition(x, y);
            orientation = Orientation;
        }
    }
}
